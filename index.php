<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
    <title>Image uploader</title>
    <meta content='' name='description'>
    <meta content='' name='author'>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1, user-scalable=no">

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link href='css/style.css' rel='stylesheet'>
  </head>
  <body>
    <div id='container'>
      <header>
        <h1>Image uploader test</h1>
      </header>
      <div id='main' role='main'></div>
      <footer></footer>
    </div>

    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')</script>
    <script src='js/main.js'></script>

  </body>
</html>
